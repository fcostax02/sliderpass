package vsgdev.com.br.sliderpass;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

/**
 * Activity responsavel por exibir e tratar os comandos do passador de slides.
 *
 * @author Franklin Soares
 */
public class SliderPassActivity extends Activity implements View.OnTouchListener {
    Button btnLeft;
    Button btnRight;
    Button btnF5;
    private Button btnHome;
    private Button btnEnd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.slider_layout);

        btnLeft = (Button) findViewById(R.id.btn_left);
        btnLeft.setOnTouchListener(this);

        btnRight = (Button) findViewById(R.id.btn_right);
        btnRight.setOnTouchListener(this);

        btnF5 = (Button) findViewById(R.id.btn_f5);
        btnF5.setOnTouchListener(this);

        btnHome = (Button) findViewById(R.id.btn_home);
        btnHome.setOnTouchListener(this);

        btnEnd = (Button) findViewById(R.id.btn_end);
        btnEnd.setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (view.equals(btnF5)){
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                UDPSenderThread.getInstance().setBuffer("4".getBytes());
            }
            if (motionEvent.getAction() == MotionEvent.ACTION_UP){
                UDPSenderThread.getInstance().setBuffer("5".getBytes());
            }
        }

        if (view.equals(btnLeft)){
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                UDPSenderThread.getInstance().setBuffer("0".getBytes());
            }
            if (motionEvent.getAction() == MotionEvent.ACTION_UP){
                UDPSenderThread.getInstance().setBuffer("1".getBytes());
            }
        }

        if (view.equals(btnRight)){
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                UDPSenderThread.getInstance().setBuffer("6".getBytes());
            }
            if (motionEvent.getAction() == MotionEvent.ACTION_UP){
                UDPSenderThread.getInstance().setBuffer("7".getBytes());
            }
        }

        if (view.equals(btnHome)) {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                UDPSenderThread.getInstance().setBuffer("8".getBytes());
            }
            if (motionEvent.getAction() == MotionEvent.ACTION_UP){
                UDPSenderThread.getInstance().setBuffer("9".getBytes());
            }
        }

        if (view.equals(btnEnd)) {
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                UDPSenderThread.getInstance().setBuffer("a".getBytes());
            }
            if (motionEvent.getAction() == MotionEvent.ACTION_UP){
                UDPSenderThread.getInstance().setBuffer("b".getBytes());
            }
        }
        return false;
    }
}
