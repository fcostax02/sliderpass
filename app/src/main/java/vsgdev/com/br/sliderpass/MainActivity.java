package vsgdev.com.br.sliderpass;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.net.InetAddress;

/**
 * Atividade Principal (MainActivity) exibida quando o app é iniciado. Trata da identificacao do computador que contém a apresentacao/slides.
 *
 * @author Franklin Soares
 */
public class MainActivity extends Activity implements View.OnClickListener{
    private EditText etServerIp;
    Button btnOkServer;
    UDPSenderThread udpSenderThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

        btnOkServer = (Button) findViewById(R.id.btn_ok_server);
        btnOkServer.setOnClickListener(this);

        etServerIp = (EditText) findViewById(R.id.et_server_ip);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (udpSenderThread != null){
            udpSenderThread.setBuffer("5".getBytes());
            udpSenderThread.running = false;
        }
    }

    @Override
    public void onClick(View view) {
        if (btnOkServer.isPressed()){
            String serverIp = etServerIp.getText().toString();

            try {
                if (udpSenderThread == null){
                    InetAddress address = InetAddress.getByName(serverIp);
                    udpSenderThread = UDPSenderThread.getInstance();
                    udpSenderThread.setServer(address);
                    udpSenderThread.start();
                }
                //TODO salvar o endereço de IP para ser mostrado em uma lista posteriormente
                startActivity(new Intent(this, SliderPassActivity.class));

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
